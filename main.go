package main
import (
	"fmt"
	"context"
	"os"
	"errors"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/gomodule/redigo/redis"

	"google.golang.org/grpc"
	"gitlab.com/Renato776/gateway/pb"
)

type Command struct {
        Stmt string `json:"stmt"`
        Token int32 `json:"token"`
}

var conn redis.Conn
var err error
var jk *grpc.ClientConn
var client pb.InterpreterClient

func forward(token int32, stmt string)( string, error ){
	if client == nil { 
		return "", errors.New("gRPC connection failed") 
	}

	req := &pb.Command{ Token: token, Stmt: stmt }
	resp, bp := client.Interpret(context.Background(), req)
	if bp != nil { return "", bp }
	if resp.Code != 200 { 
		return "", errors.New(resp.Body) 
	} else { return resp.Body, nil }
}

func Interpret(ctx context.Context, c Command) (string, error) {
	if conn == nil { 
	  return "", errors.New("Connection to either server failed.") 
  	}
	var ren string
	ren,err = redis.String(conn.Do("HGET",fmt.Sprintf("core:%v",c.Token),"usr"))
	if err != nil { return "", 
	  errors.New("Session validation failed.") }

  	ren,err = redis.String(conn.Do("HGET",fmt.Sprintf("cache:%v",c.Token),c.Stmt))
	if err != nil { 
		fmt.Printf("Forwarding request: %v,%v\n",c.Token,c.Stmt)
		return forward( c.Token, c.Stmt ) 
	} else { 
	  fmt.Printf("Serving cached response for: %v,%v\n",c.Token, c.Stmt)
	  return ren, nil 
	}
}

func init() { 
	addr := os.Getenv("GRPC_HOST")+":"+os.Getenv("GRPC_PORT")
	jk, err = grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil { panic ( err ) }
	client = pb.NewInterpreterClient(jk)

	conn,err = redis.Dial("tcp",
	os.Getenv("REDIS_HOST")+":"+os.Getenv("REDIS_PORT"))
	if err != nil {
		panic(err)
	}
}

func terminar(){
	if conn != nil { conn.Close() }
	if jk != nil { jk.Close() }
}
func main() {
        lambda.Start(Interpret)
	defer terminar()
}
