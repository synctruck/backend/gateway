module gitlab.com/Renato776/gateway

go 1.13

require (
	github.com/aws/aws-lambda-go v1.20.0
	github.com/golang/protobuf v1.4.1
	github.com/gomodule/redigo v2.0.0+incompatible
	google.golang.org/grpc v1.33.2
	google.golang.org/protobuf v1.25.0
)
